'use strict';
/**
 * @ngdoc overview
 * @name bilApp
 * @description
 * # ngApp
 * 
 * v0.0.1   
 *
 * Main module of the application.
 */
angular.module('ngApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
]).config(["$routeProvider", function ($routeProvider) {
    $routeProvider
        .when('/', {
            templateUrl: 'screens/1.html',
            controller: 'ExmController',
            controllerAs: 'main'
        })
        .when('/2', {
            templateUrl: 'screens/2.html',
            controller: 'ExmController',
            controllerAs: 'main'
        })
        .when('/3', {
            templateUrl: 'screens/3.html',
            controller: 'ExmController',
            controllerAs: 'main'
        })
        .when('/4', {
            templateUrl: 'screens/4.html',
            controller: 'ExmController',
            controllerAs: 'main'
        })
        .when('/5', {
            templateUrl: 'screens/5.html',
            controller: 'ExmController',
            controllerAs: 'main'
        })
        .when('/6', {
            templateUrl: 'screens/6.html',
            controller: 'ExmController',
            controllerAs: 'main'
        })
        .otherwise({
            redirectTo: '/'
        });
}]);
